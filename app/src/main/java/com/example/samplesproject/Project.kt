package com.example.samplesproject

import androidx.annotation.StringRes

enum class Project(@StringRes val moduleName: Int) {
    COLLAPSING_TOOLBAR(R.string.module_collapsing_toolbar)
}