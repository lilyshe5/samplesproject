package com.example.samplesproject

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.samplesproject.databinding.ItemProjectBinding
import kotlinx.android.extensions.LayoutContainer

class ProjectsAdapter(private val projectAdapterListener: ProjectAdapterListener) :
    ListAdapter<Project, ProjectsAdapter.ProjectsViewHolder>(DIFF_UTIL) {

    companion object {
        private val DIFF_UTIL = object :
            DiffUtil.ItemCallback<Project>() {
            override fun areItemsTheSame(oldItem: Project, newItem: Project): Boolean =
                oldItem == newItem

            override fun areContentsTheSame(oldItem: Project, newItem: Project): Boolean =
                oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProjectsViewHolder =
        ProjectsViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_project, parent, false)
        )

    override fun onBindViewHolder(holder: ProjectsViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ProjectsViewHolder(
        override val containerView: View
    ) : RecyclerView.ViewHolder(containerView), LayoutContainer {

        private val binding = ItemProjectBinding.bind(containerView)

        init {
            binding.buttonProject.setOnClickListener {
                projectAdapterListener.onProjectClicked(getItem(adapterPosition))
            }
        }

        fun bind(project: Project) {
            binding.buttonProject.text = containerView.resources.getString(project.moduleName)
        }
    }

    interface ProjectAdapterListener {
        fun onProjectClicked(project: Project)
    }
}