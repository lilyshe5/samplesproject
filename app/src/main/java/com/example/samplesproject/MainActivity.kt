package com.example.samplesproject

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.samplesproject.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(R.layout.activity_main),
    ProjectsAdapter.ProjectAdapterListener {

    private val binding by viewBinding(ActivityMainBinding::bind, R.id.rootLayout)

    private val projectsAdapter = ProjectsAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initView()
    }

    private fun initView() = with(binding) {
        recyclerProjects.apply {
            adapter = projectsAdapter
            addItemDecoration(
                VerticalItemDecoration(
                    horizontalMargin = 16.dpToPx,
                    verticalMargin = 8.dpToPx,
                    firstMarginTop = 12.dpToPx,
                    lastMarginBottom = 12.dpToPx
                )
            )
        }
    }

    override fun onProjectClicked(project: Project) = when (project) {
        Project.COLLAPSING_TOOLBAR -> {
        }
    }
}