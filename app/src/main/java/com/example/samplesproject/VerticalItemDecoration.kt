package com.example.samplesproject

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class VerticalItemDecoration(
    private val horizontalMargin: Int,
    private val verticalMargin: Int,
    private val firstMarginTop: Int,
    private val lastMarginBottom: Int
) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        super.getItemOffsets(outRect, view, parent, state)
        val itemPosition = parent.getChildAdapterPosition(view)

        if (itemPosition == RecyclerView.NO_POSITION) return

        outRect.left = horizontalMargin
        outRect.right = horizontalMargin

        outRect.top = when (itemPosition) {
            0 -> firstMarginTop
            else -> verticalMargin / 2
        }

        outRect.bottom = when {
            itemPosition == 0 -> verticalMargin / 2
            state.itemCount > 0 && itemPosition == state.itemCount - 1 -> lastMarginBottom
            else -> verticalMargin / 2
        }
    }
}